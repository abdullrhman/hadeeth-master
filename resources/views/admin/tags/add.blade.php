@extends('layouts.app_admin')

@section('title')
Dashboard - Add Tag
@endsection
@section('extra-css')

@endsection

@section('content')
<div >
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">New Tag</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New Tag
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            @if (Session::has('success'))
                            <div class="alert alert-success">
                                <ul>
                                    <li class="list-unstyled"> 
                                        {!! Session::get('success') !!}
                                    </li>
                                </ul>
                            </div>
                            @endif
                            
                            @if (Session::has('fail'))
                            <div class="alert alert-danger">
                                <ul>
                                    <li class="list-unstyled">
                                        {!! Session::get('fail') !!}
                                    </li>
                                </ul>
                            </div>
                            @endif
                            <form role="form" action="{{ route('tag') }}" method='POST'>
                                @csrf
                                <div class="form-group">
                                    <label>Tag Name</label>
                                    <input class="form-control" name='name' placeholder="Enter Tag Title">
                                </div>
                                <button type="submit" class="btn btn-default">Submit Button</button>
                                <button type="reset" class="btn btn-default">Reset Button</button>                              
                            </form>
                        </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
@endsection

@section('extra-js')

@endsection