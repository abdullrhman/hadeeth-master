@extends('layouts.app_admin')

@section('title')
Dashboard - Add Hadith
@endsection
@section('extra-css')
<style>
.photo-box{
    height:300px;
    overflow: hidden;
}
.photo-box img{
    height:100%;
}
.reference-box{
    
}
.reference-box img{
    
}
</style>
@endsection

@section('content')
<div >
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{$hadith->title}}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row" id="image">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Hadith {{$hadith->type}}
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>{{$hadith->body}}</p>
                                </div>
                                <!-- /.col-lg-12 (nested) -->
                            </div>
                            <div class="row">
                                @if(isset($images))
                                    @foreach($images as $image)
                                    <div class="col-lg-4 col-sm-12">
                                        <div class="photo-box">
                                            <img class="img-thumbnail" width="100%" alt="{{$image->description}}" title="{{$image->description}}" src="{{url($image->image)}}">
                                        </div>
                                        <a href="javascript::void(0)" onclick="deleteItem({{$image->id}})" type="button" class="close" aria-label="Close">
                                            <span aria-hidden="true">&times;</span> Delete
                                        </a>
                                    </div>
                                    @endforeach
                                @endif
                                <!-- /.col-lg-12 (nested) -->
                            </div>
                            <div class="row">
                                @if(isset($references))
                                    @foreach($references as $reference)
                                    <div class="col-lg-4 col-sm-12">
                                        <div class="reference-box">
                                            <img class="img-thumbnail" width="100%" alt="{{$reference->description}}" title="{{$reference->description}}" src="{{url($reference->image)}}">
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                                <!-- /.col-lg-12 (nested) -->
                            </div>
                        </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<div id="dialog" class="row hide">
    <div id="dialog" class="col-lg-12">
        <section class="panel panel-default">
            <header class="panel-heading">
                <h2 class="panel-title">Are you sure?</h2>
            </header>
            <div class="panel-body">
                <div class="modal-wrapper">
                    <div class="modal-text">
                        <p>Are you sure that you want to delete this Photo ?</p>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button onclick="dialogConfirm()" id="dialogConfirm" class="btn btn-primary">Confirm</button>
                        <button onclick="dialogCancel()" id="dialogCancel" class="btn btn-default">Cancel</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>
</div>
<!-- /#page-wrapper -->
@endsection

@section('extra-js')
<script>
var _self = this;
var action = null;
var id = null;
var table = document.querySelector("#image");
var dialog = document.querySelector("#dialog");
function deleteItem(id){
    dialog.classList.remove('hide');
    table.classList.add('hide');
    _self.id = id;
    _self.action = function(){
        $.ajax({
            url: '/admin/photo/'+_self.id,
            type: 'GET',
            success: function(result) {
                alert("done");
                console.log(result);
            }
        });
        location.reload();
    }
}
function dialogConfirm(){
    table.classList.remove('hide');
    dialog.classList.add('hide');
    _self.action();
    _self.action = null;
    _self.id = null;
}
function dialogCancel(){
    table.classList.remove('hide');
    dialog.classList.add('hide');
    _self.action = null;
    _self.id = null;
}

</script>
@endsection