@extends('layouts.app_admin')

@section('title')
Dashboard - Add Hadith
@endsection
@section('extra-css')

@endsection

@section('content')
<div >
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Hadiths</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row" id="table">
        <div class="col-lg-12">
            @if (Session::has('success'))
            <div class="alert alert-success">
                <ul>
                    <li class="list-unstyled"> 
                        {!! Session::get('success') !!}
                    </li>
                </ul>
            </div>
            @endif

            @if (Session::has('fail'))
            <div class="alert alert-danger">
                <ul>
                    <li class="list-unstyled">
                        {!! Session::get('fail') !!}
                    </li>
                </ul>
            </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">
                    Hadiths
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>title</th>
                                <th>type</th>
                                <th>action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($hadiths as $hadith)
                            <tr class="odd gradeX">
                                <th>{{$hadith->id}}</th>
                                <td><a href="{{url('/admin/hadith/'.$hadith->id)}}">{{$hadith->title}}</a></td>
                                <td>{{$hadith->type}}</td>
                                <td class="actions-fade">
                                    <a class="btn" onclick="deleteItem({{$hadith->id}})"><i class="fa fa-trash-o"></i></a>
                                    <a class="btn" href="{{url('/admin/hadith/edit/'.$hadith->id)}}"><i class="fa fa-pencil"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="4">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            {!! $hadiths->render() !!}
                                        </div>
                                    </div>
                                </th>
                            </tr>
                        </tfoot>

                    </table>
                    <!-- /.table-responsive -->
                </div>

                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

</div>
<div id="dialog" class="row hide">
    <div id="dialog" class="col-lg-12">
        <section class="panel panel-default">
            <header class="panel-heading">
                <h2 class="panel-title">Are you sure?</h2>
            </header>
            <div class="panel-body">
                <div class="modal-wrapper">
                    <div class="modal-text">
                        <p>Are you sure that you want to delete this Hadith ?</p>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button onclick="dialogConfirm()" id="dialogConfirm" class="btn btn-primary">Confirm</button>
                        <button onclick="dialogCancel()" id="dialogCancel" class="btn btn-default">Cancel</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>
</div>
<!-- /#page-wrapper -->
@endsection

@section('extra-js')
<!-- DataTables JavaScript -->
<script src="{{url('/dashboard/vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('/dashboard/vendor/datatables-plugins/dataTables.bootstrap.min.js')}}"></script>
<script src="{{url('/dashboard/vendor/datatables-responsive/dataTables.responsive.js')}}"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
var _self = this;
var action = null;
var id = null;
var table = document.querySelector("#table");
var dialog = document.querySelector("#dialog");
$(document).ready(function () {
    $('#dataTables').DataTable({
        "responsive": true,
        "paging": false,
        "lengthMenu": [10],
        "dom": '<"top col-lg-12"><"bottom"flp><"clear">'
    });
});
function deleteItem(id){
    dialog.classList.remove('hide');
    table.classList.add('hide');
    _self.id = id;
    _self.action = function(){
        $.ajax({
            url: '/admin/hadith/'+_self.id,
            data: {
                    "_token": "{{ csrf_token() }}",
                    },
            type: 'DELETE',
            success: function(result) {
                alert("done");
                console.log(result);
            }
        });
        location.reload();
    }
}
function dialogConfirm(){
    table.classList.remove('hide');
    dialog.classList.add('hide');
    _self.action();
    _self.action = null;
    _self.id = null;
}
function dialogCancel(){
    table.classList.remove('hide');
    dialog.classList.add('hide');
    _self.action = null;
    _self.id = null;
}

</script>
@endsection

