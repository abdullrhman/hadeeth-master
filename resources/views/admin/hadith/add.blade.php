@extends('layouts.app_admin')

@section('title')
Dashboard - Add Hadith
@endsection
@section('extra-css')

@endsection

@section('content')
<div >
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">New Hadith</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New Hadith 
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                           @if (Session::has('success'))
                            <div class="alert alert-success">
                                <ul>
                                    <li class="list-unstyled"> 
                                        {!! Session::get('success') !!}
                                    </li>
                                </ul>
                            </div>
                            @endif
                            
                            @if (Session::has('fail'))
                            <div class="alert alert-danger">
                                <ul>
                                    <li class="list-unstyled">
                                        {!! Session::get('fail') !!}
                                    </li>
                                </ul>
                            </div>
                            @endif
                            <form role="form" method="POST" action="{{route('hadith')}}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label>Hadith Title</label>
                                    <input name="title" class="form-control" placeholder="Enter Hadith Title">
                                    @if ($errors->has('title'))                               
                                    <strong class="col-sm-6 center text-danger">{{ $errors->first('title') }}</strong>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Hadith Body</label>
                                    <textarea name="body" class="form-control" rows="5" placeholder="Enter Hadith body"></textarea>
                                    @if ($errors->has('body'))                               
                                    <strong class="col-sm-6 center text-danger">{{ $errors->first('body') }}</strong>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Hadith Type</label>
                                    <select class="form-control" name="type">
                                        @foreach($types as $type)
                                        <option value="{{$type}}">{{$type}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('type'))                               
                                    <strong class="col-sm-6 center text-danger">{{ $errors->first('type') }}</strong>
                                    @endif
                                </div>

                                <div class="form-group" >
                                    <label>Hadith Categories</label>
                                    <select class="form-control" name="category">
                                        @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('category'))                               
                                    <strong class="col-sm-6 center text-danger">{{ $errors->first('category') }}</strong>
                                    @endif
                                </div>
                                
                                <div class="form-group" >
                                    <label>Tags</label>
                                    <select class="form-control" name="tags[]" multiple>
                                        @foreach($tags as $tag)
                                        <option value="{{$tag->id}}">{{$tag->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('tags'))                               
                                    <strong class="col-sm-6 center text-danger">{{ $errors->first('tags') }}</strong>
                                    @endif
                                </div>
                                <div class="form-group" >
                                    <label>Reference Pages</label>
                                    <select class="form-control" name="pages[]" multiple>
                                        @foreach($pages as $page)
                                        <option value="{{$page->id}}">{{$page->description}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('tags'))                               
                                    <strong class="col-sm-6 center text-danger">{{ $errors->first('tags') }}</strong>
                                    @endif
                                </div>
                                <div class="form-group" >
                                    <label>Images</label>
                                    <input type="file" class="form-control" name="image[]" multiple/>
                                    @if ($errors->has('image'))                               
                                    <strong class="col-sm-6 center text-danger">{{ $errors->first('image') }}</strong>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-default">Submit Button</button>
                                <button type="reset" class="btn btn-default">Reset Button</button>                              
                            </form>
                        </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
@endsection

@section('extra-js')

@endsection