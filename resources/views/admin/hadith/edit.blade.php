@extends('layouts.app_admin')

@section('title')
Dashboard - Add Hadith
@endsection
@section('extra-css')

@endsection

@section('content')
<div >
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">New Hadith</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New Hadith 
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                           @if (Session::has('success'))
                            <div class="alert alert-success">
                                <ul>
                                    <li class="list-unstyled"> 
                                        {!! Session::get('success') !!}
                                    </li>
                                </ul>
                            </div>
                            @endif
                            
                            @if (Session::has('fail'))
                            <div class="alert alert-danger">
                                <ul>
                                    <li class="list-unstyled">
                                        {!! Session::get('fail') !!}
                                    </li>
                                </ul>
                            </div>
                            @endif
                            <form role="form" action="{{ route('hadith.update',$hadith->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                {{ method_field('PATCH') }}
                                <input type="hidden" name="id" value="{{$hadith->id}}">
                                <div class="form-group">
                                    <label>Hadith Title</label>
                                    <input name="title" class="form-control" placeholder="Enter Hadith Title" value="{{$hadith->title}}">
                                    @if ($errors->has('title'))                               
                                    <strong class="col-sm-6 center text-danger">{{ $errors->first('title') }}</strong>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Hadith Body</label>
                                    <textarea name="body" class="form-control" rows="5" placeholder="Enter Hadith body">{{$hadith->body}}</textarea>
                                    @if ($errors->has('body'))                               
                                    <strong class="col-sm-6 center text-danger">{{ $errors->first('body') }}</strong>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Hadith Type</label>
                                    <select class="form-control" name="type">
                                        @foreach($types as $type)
                                            @if($type == $hadith->type)
                                                <option selected="selected" value="{{$type}}">{{$type}}</option>
                                            @else
                                                <option value="{{$type}}">{{$type}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    @if ($errors->has('type'))                               
                                    <strong class="col-sm-6 center text-danger">{{ $errors->first('type') }}</strong>
                                    @endif
                                </div>

                                <div class="form-group" >
                                    <label>Hadith Categories</label>
                                    <select class="form-control" name="category">
                                        @foreach($categories as $category)
                                            @if($category->id == $hadith->category_id)
                                                <option selected="selected" value="{{$category->id}}">{{$category->name}}</option>
                                            @else
                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    @if ($errors->has('category'))                               
                                    <strong class="col-sm-6 center text-danger">{{ $errors->first('category') }}</strong>
                                    @endif
                                </div>
                                
                                <div class="form-group" >
                                    <label>Tags</label>
                                    <select class="form-control" name="tags[]" multiple>
                                        @foreach($tags as $tag)
                                            @if(in_array($tag->id , $hadith->tags_ids))
                                                <option selected="selected" value="{{$tag->id}}">{{$tag->name}}</option>
                                            @else
                                                <option value="{{$tag->id}}">{{$tag->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                
                                <div class="form-group" >
                                    <label>Reference Pages</label>
                                    <select class="form-control" name="pages[]" multiple>
                                        @foreach($pages as $page)
                                        @if(in_array($page->id , $hadith->pages_ids))
                                        <option selected="selected" value="{{$page->id}}">{{$page->description}}</option>
                                        @else
                                        <option value="{{$page->id}}">{{$page->description}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                                
                                <div class="form-group" >
                                    <label>Images</label>
                                    <input type="file" class="form-control" name="image[]" multiple/>
                                    @if ($errors->has('image'))                               
                                    <strong class="col-sm-6 center text-danger">{{ $errors->first('image') }}</strong>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-default">Submit Button</button>
                                <button type="reset" class="btn btn-default">Reset Button</button>                              
                            </form>
                        </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
@endsection

@section('extra-js')

@endsection