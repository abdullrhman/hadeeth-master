@extends('layouts.admin')

@section('title')
Dr Assistant - Home - Admin Panel
@endsection
@section('extra-css')

@endsection
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>New User</h2>
    </header>
    <!-- start: page -->
    <div class="row">
        <div class="col-md-12">
            <form  action="{{ url('/adminPanel/storeDoctor') }} " class="form-horizontal" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <section class="panel">
                    <header class="panel-heading">
                        @if(session()->has('success'))
                        <h4 class="alert alert-success">
                            {{ session()->get('success') }}
                        </h4>
                        @endif
                        @if(session()->has('error'))
                        <div class="alert alert-danger">
                            {{ session()->get('error') }}
                        </div>
                        @endif
                    </header>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Gender</label>
                            <div class="col-sm-9">
                                <select name="gender" id="gender" class="form-control" title="Please select your gender" required>     
                                        <option selected="selected" value="0">male</option>
                                        <option value="1">female</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">First Name <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" name="first_name" placeholder="First Name" required/>
                            </div>
                            @if ($errors->has('first_name'))                               
                            <strong class="col-sm-6 center text-danger">{{ $errors->first('first_name') }}</strong>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Last Name <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" name="last_name"  placeholder="Last Name" required/>
                            </div>
                            @if ($errors->has('last_name'))                               
                            <strong class="col-sm-6 center text-danger">{{ $errors->first('last_name') }}</strong>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope"></i>
                                    </span>
                                    <input type="email" name="email" class="form-control" placeholder="eg.: email@email.com" required/>
                                    @if ($errors->has('email'))                               
                                    <strong class="col-sm-6 center text-danger">{{ $errors->first('email') }}</strong>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-9">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"> Password <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="password" name="password" class="form-control" placeholder="password" required/>
                            </div>
                            @if ($errors->has('password'))                               
                            <strong class="col-sm-6 center text-danger">{{ $errors->first('password') }}</strong>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"> confirm password <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="password" name="password_confirmation" class="form-control" placeholder="password" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Specialty</label>
                            <div class="col-sm-9">
                                <select name="speciality" id="speciality" class="form-control" title="Please select one specialty" required>
                                    @if (!$specialities)
                                    <option  disabled="disabled" selected>there are no specialities yet</option>
                                    @endif
                                    @if ($specialities)
                                    <option value="" selected="selected">Choose One Speciality</option>
                                    @foreach ($specialities as $speciality)
                                    <option value="{{$speciality->id}}">{{$speciality->name}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Country</label>
                            <div class="col-sm-9">
                                <select onchange="getCities()" value="" id="country" name="country" class="form-control" title="Please select your country" required>
                                    @if (!$countries)
                                    <option  disabled="disabled" selected>there are no countries yet</option>
                                    @endif
                                    @if ($countries)
                                    <option value="" selected="selected">Choose One Country</option>
                                    @foreach ($countries as $country)
                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="city_holder">
                            <label class="col-sm-3 control-label">city</label>
                            <div class="col-sm-9">
                                <select id="city" name="city" disabled="disabled" class="form-control" title="Please select your city" required>
                                    <option value="">choose Country first</option>
                                </select>
                            </div>

                        </div> 
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Mobile</label>
                            <div class="col-sm-9">
                                <input type="phone" name="phone" class="form-control" placeholder="mobile number" maxlength="11"/>
                            </div>
                            @if ($errors->has('phone'))                               
                            <strong class="col-sm-6 center text-danger">{{ $errors->first('phone') }}</strong>
                            @endif
                        </div> 
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Address</label>
                            <div class="col-sm-9">
                                <input type="text" name="address" class="form-control" placeholder="Street no. Block no. ,City ,Area" />
                            </div>
                            @if ($errors->has('address'))                               
                            <strong class="col-sm-6 center text-danger">{{ $errors->first('address') }}</strong>
                            @endif
                        </div>
                        <div class="form-group-custom">
                            <label class="form-label" for="user-license">license</label>
                            <input type="file" id="user-license" name="license" required="required"/>
                        </div>
                        @if ($errors->has('license'))                               
                        <strong class="col-sm-6 center text-danger">{{ $errors->first('license') }}</strong>
                        @endif
                    </div>
                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn btn-default">Reset</button>
                            </div>
                        </div>
                    </footer>
                </section>
            </form>
        </div> 
    </div>
    <!-- end: page -->
</section>
@endsection
@section('extra-js')
<!-- Specific Page Vendor -->
<script src="{{url('/custom/register.js')}}"></script>
<script src="{{url('/admin_dashboard/assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{url('/admin_dashboard/assets/vendor/select2/js/select2.js')}}"></script>
<!-- Examples -->
<script src="{{url('/admin_dashboard/assets/javascripts/forms/add.validation.js')}}"></script>
<script>
                                    /* $(document).ready(function () {
                                     $.get("{{ url('/adminPanel/getAllUsers') }}", function (data) {
                                     console.log(data);
                                     });
                                     
                                     });*/
</script>
@endsection