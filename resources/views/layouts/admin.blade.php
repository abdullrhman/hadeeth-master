<!DOCTYPE html>
<html lang="en" class="fixed">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title')</title>
        @include('assets.admin.head')
        @yield('extra-css')
    </head>
    <body>
        <section class="body">
            @include('assets.admin.header')
            <div class="inner-wrapper">
                @include('assets.admin.sidebar')
                @yield('content')  
            </div>
        </section>
        @include('assets.admin.js')
        @yield('extra-js')
    </body>
</html>