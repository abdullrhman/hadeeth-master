<!DOCTYPE html>
<html lang="en">
    <head>
        @include('assets.meta')
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title')</title>
        @include('assets.css')
        @yield('extra-css')
    </head>
    <body>
        <!-- wrapper -->
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                @include('assets.navigation')
                @include('assets.sidebar.admin')
            </nav>

            <!-- /#Navigation -->
            <!-- page-wrapper -->
            <div id="page-wrapper">
                @yield('content')    
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        @include('assets.js')
        @yield('extra-js')
    </body>
</html>
