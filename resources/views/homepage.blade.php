@extends('layouts.app')

@section('title')
    Hedeeth - Home
@endsection

@section('content')
    @admin
    @include('admin.dashboard')
    @admin

    @editor
    @include('editor.dashboard')
    @editor

@endsection
