<!-- jQuery -->
<script src="{{url('/dashboard/vendor/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{url('/dashboard/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="{{url('/dashboard/vendor/metisMenu/metisMenu.min.js')}}"></script>

<!-- Custom Theme JavaScript -->
<script src="{{url('/dashboard/dist/js/sb-admin-2.js')}}"></script>
