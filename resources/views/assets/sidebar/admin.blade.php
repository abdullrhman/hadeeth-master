<!-- navbar-static-side -->
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
                <!-- /input-group -->
            </li>
            <li>
                <a href="{{url('/admin')}}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{url('/admin/tags')}}"><i class="fa fa-edit fa-fw"></i> Tags<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{url('/admin/tags/create')}}">New Tag</a>
                    </li>
                    <li>
                        <a href="{{url('/admin/tags')}}">All Tags</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{url('/admin/categories')}}"><i class="fa fa-sitemap fa-fw"></i> Categories <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{url('/admin/categories/main')}}"> Main Category <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="{{url('/admin/categories/main/create')}}">Add Main Category</a>
                            </li>
                            <li>
                                <a href="{{url('/admin/categories/main')}}">All Main Categories</a>
                            </li>
                        </ul>
                        <!-- /.nav-third-level -->
                    </li>
                    <li>
                        <a href="{{url('/admin/categories/sub')}}"> Sub Category <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="{{url('/admin/categories/sub/create')}}">Add Sub Category</a>
                                </li>
                                <li>
                                    <a href="{{url('/admin/categories/sub')}}">All Sub Categories</a>
                                </li>
                            </ul>
                        </ul>
                        <!-- /.nav-third-level -->
                    </li>
                    <li>
                        <a href="{{url('/admin/categories/sub')}}"> Section Category <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="{{url('/admin/categories/section/create')}}">Add Section Category</a>
                                </li>
                                <li>
                                    <a href="{{url('/admin/categories/section')}}">All Section Categories</a>
                                </li>
                            </ul>
                        </ul>
                        <!-- /.nav-third-level -->
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="{{url('/admin/hadith')}}"><i class="fa fa-edit fa-fw"></i> Hadiths<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{url('/admin/hadith/create')}}">New Hadith</a>
                    </li>
                    <li>
                        <a href="{{url('/admin/hadith')}}">All hadiths</a>
                    </li>
                </ul>
            </li>
            
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /#navbar-static-side -->
