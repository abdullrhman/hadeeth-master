<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagSubscription extends Model
{
    protected $table = "tags_subscription";
}
