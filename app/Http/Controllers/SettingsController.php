<?php

namespace App\Http\Controllers;

use App\User;
use DotEnvEditor\DotenvEditor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Mockery\Exception;
use Validator;

class SettingsController extends Controller {

    public function resetPassword() {
        return view('admin.settings.reset_password');
    }

    public function changePassword(Request $request) {
        $validator = Validator::make($request->all(),[
            'password' => 'required|min:5',
            'new_password' => 'required|min:5',
            'confirm' => 'required|same:new_password'
        ]);
        if ($validator->fails()) {
           return redirect()->back()->withErrors($validator);
        }
        
        if (Hash::check($request->get('password'), auth()->user()->password)) {
            $user = User::find(auth()->user()->id);
            $user->password = Hash::make($request->get('new_password'));
            if ($user->save()) {
                return redirect()->back()->with('success', 'Password Changed');
            } else {
                return redirect()->back()->with('fail', "fail");
            }
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * Configure cache
     */
    public function cacheConfig() {
        Artisan::call('config:cache');
        return 'success';
    }

    public function installSuccess() {
        if (User::all()) {
            $_env = new DotenvEditor();
            $_env->changeEnv([
                'HAS_INSTALLED' => 1
            ]);
            Artisan::call('config:cache');
            return view('install.install-success');
        } else {
            return redirect()->to('/install');
        }
    }

}
