<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Hadith;
use App\Category;
use App\Tag;
use App\TagSubscription;
use App\Photo;
use App\SahihAlbukhari;
use App\SahihAlbukhariSubscription;

class HadithsController extends Controller {

    protected $types = ['alqudsiu', 'almarfue', 'almawquf', 'almaqtue', 'alsahih', 'aldaeif'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $hadiths = Hadith::paginate(10);
        return view('admin.hadith.all')->with('hadiths', $hadiths, 'types', $this->types);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $types = $this->types;
        $categories = Category::where('parent_id', '!=', null)->where('second_parent_id', '!=', null)->get();
        $tags = Tag::all();
        $pages = SahihAlbukhari::all();
        return view('admin.hadith.add')
                        ->with('categories', $categories)
                        ->with('types', $types)
                        ->with('tags', $tags)
                        ->with('pages', $pages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'title' => 'required|string',
                    'body' => 'required|string',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
        }
        $hadith = new Hadith();
        $hadith->title = $request->get('title');
        $hadith->body = $request->get('body');
        $hadith->type = $request->get('type');
        $hadith->category_id = $request->get('category');
        if ($hadith->save()) {
            if ($request->hasFile('image')) {
                $images = $request->file('image');
                $photos = [];
                for ($i = 0; $i < count($images); $i++) {
                    $photos[$i]["image"] = $images[$i]->move('uploads/images', $request->type . "_" . rand(100000, 900000) . '.' . $images[$i]->extension());
                    $photos[$i]["hadith_id"] = $hadith->id;
                    $photos[$i]["description"] = $hadith->title;
                    $created_time = Carbon::now();
                    $photos[$i]["updated_at"] = $created_time;
                    $photos[$i]["created_at"] = $created_time;
                }
                Photo::insert($photos);
            }
            if ($request->has('tags')) {
                $tags = $request->get('tags');
                $Newtags = [];
                for ($i = 0; $i < count($tags); $i++) {
                    $Newtags[$i]["hadith_id"] = $hadith->id;
                    $Newtags[$i]["tag_id"] = $tags[$i];
                }
                TagSubscription::insert($Newtags);
            }
            if ($request->has('pages')) {
                $pages = $request->get('pages');
                $newPages = [];
                for ($i = 0; $i < count($pages); $i++) {
                    $newPages[$i]["hadith_id"] = $hadith->id;
                    $newPages[$i]["page_id"] = $pages[$i];
                    $created_time = Carbon::now();
                    $newPages[$i]["updated_at"] = $created_time;
                    $newPages[$i]["created_at"] = $created_time;
                }
                SahihAlbukhariSubscription::insert($newPages);
            }

            return redirect()->back()->with('success', 'success');
        } else {
            return redirect()->back()->with('fail', "fail");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hadith  $hadith
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $hadith = Hadith::findOrFail($id);
        $images = Photo::where('hadith_id', $id)->get();
        $references_ids = SahihAlbukhariSubscription::where('hadith_id', $id)->get()->pluck('id')->toArray();
        $references = SahihAlbukhari::whereIn('id', $references_ids)->get();
        return view('admin.hadith.show')->with('hadith', $hadith)->with('references', $references)->with('images', $images);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hadith  $hadith
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $hadith = DB::table('hadiths')->where('hadiths.id', $id)
                        ->join('categories_subscription', 'categories_subscription.hadith_id', '=', 'hadiths.id')
                        ->select('hadiths.*', 'categories_subscription.category_id')->get()->first();
        $hadith->tags_ids = TagSubscription::where('hadith_id', $id)->get()->pluck('tag_id')->toArray();
        $hadith->pages_ids = SahihAlbukhariSubscription::where('hadith_id', $id)->get()->pluck('page_id')->toArray();
        $types = $this->types;
        $categories = Category::all();
        $tags = Tag::all();
        $pages = SahihAlbukhari::all();

        return view('admin.hadith.edit')
                        ->with('categories', $categories)
                        ->with('types', $types)
                        ->with('tags', $tags)
                        ->with('pages', $pages)
                        ->with('hadith', $hadith);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hadith  $hadith
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
                    'title' => 'required|string',
                    'body' => 'required|string',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
        }

        $hadith = Hadith::findOrFail($id);

        $hadith->title = $request->get('title');
        $hadith->body = $request->get('body');
        $hadith->type = $request->get('type');
        $hadith->category_id = $request->get('category');
        if ($hadith->save()) {
            if ($request->hasFile('image')) {
                $images = $request->file('image');
                $photos = [];
                for ($i = 0; $i < count($images); $i++) {
                    $photos[$i]["image"] = $images[$i]->move('uploads/images', $request->title . "_" . rand(100000, 900000) . '.' . $images[$i]->extension());
                    $photos[$i]["hadith_id"] = $hadith->id;
                    $photos[$i]["description"] = $hadith->title;
                    $created_time = Carbon::now();
                    $photos[$i]["updated_at"] = $created_time;
                    $photos[$i]["created_at"] = $created_time;
                }
                Photo::insert($photos);
            }
            if ($request->has('tags')) {
                $tagsRequest = $request->get('tags');
                $oldTags = TagSubscription::where('hadith_id', $id)->pluck('tag_id')->toArray();
                $newTags = $tagsRequest;
                if (!is_array($newTags)) {
                    $newTags = [];
                }
                if (!is_array($oldTags)) {
                    $oldTags = [];
                }
                if (is_array($newTags) && is_array($oldTags)) {
                    $dataSet = [];
                    $oldTags = array_map(function($e) {
                        return (int) $e;
                    }, $oldTags);
                    $newTags = array_map(function($e) {
                        return (int) $e;
                    }, $newTags);

                    $removeTags = array_diff($oldTags, $newTags);
                    $addTags = array_diff($newTags, $oldTags);
                    if (count($addTags) > 0) {
                        foreach ($addTags as $tag) {
                            $dataSet[] = [
                                'hadith_id' => $id,
                                'tag_id' => $tag,
                            ];
                        }
                        TagSubscription::insert($dataSet);
                    }
                    if (count($removeTags) > 0) {
                        TagSubscription::where('hadith_id', $id)->whereIn('tag_id', $removeTags)->delete();
                    }
                }
            }
            if ($request->has('pages')) {
                $pagesRequest = $request->get('pages');
                $oldPages = SahihAlbukhariSubscription::where('hadith_id', $id)->pluck('page_id')->toArray();
                $newPages = $pagesRequest;
                if (!is_array($newPages)) {
                    $newPages = [];
                }
                if (!is_array($oldPages)) {
                    $oldPages = [];
                }
                if (is_array($newPages) && is_array($oldPages)) {
                    $dataBukhari = [];
                    $oldPages = array_map(function($e) {
                        return (int) $e;
                    }, $oldPages);
                    $newPages = array_map(function($e) {
                        return (int) $e;
                    }, $newPages);

                    $removePages = array_diff($oldPages, $newPages);
                    $addPages = array_diff($newPages, $oldPages);
                    if (count($addPages) > 0) {
                        foreach ($addPages as $page) {
                            $dataBukhari[] = [
                                'hadith_id' => $id,
                                'page_id' => $page,
                            ];
                        }
                        SahihAlbukhariSubscription::insert($dataBukhari);
                    }
                    if (count($removePages) > 0) {
                        SahihAlbukhariSubscription::where('hadith_id', $id)->whereIn('page_id', $removePages)->delete();
                    }
                }
            }
            return redirect()->back()->with('success', 'success adding hadith but without updating subscription');
        } else {
            return redirect()->back()->with('fail', "fail");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hadith  $hadith
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (Hadith::destroy($id)) {
            TagSubscription::where('hadith_id', $id)->delete();
            Photo::where('hadith_id', $id)->delete();
            SahihAlbukhariSubscription::where('hadith_id', $id)->delete();
            return redirect()->back()->with('success', 'Delete Success');
        } else {
            return redirect()->back()->with('fail', "Delete Fail");
        }
    }

    protected function getPhotosByHadithsIds($hadiths_ids) {
        return DB::table('sahih_albukhari_subscription')->whereIn('sahih_albukhari_subscription.hadith_id', $hadiths_ids)
                        ->join('sahih_albukhari', 'sahih_albukhari.id', '=', 'sahih_albukhari_subscription.page_id')
                        ->select('sahih_albukhari_subscription.*', 'sahih_albukhari.*')->get()->toArray();
    }

    public function getHadithsByType(request $request) {
        if ($request->has('type')) {
            $type = $request->get('type');
        } else {
            return response([], 200);
        }
        if ($request->has('limit')) {
            $limit = $request->get('limit');
        } else {
            $limit = 5;
        }
        if (!$request->has('page') || $request->get('page') < 1) {
            $page = 1;
        } else {
            $page = $request->get('page');
        }
        $hadiths_count = Hadith::where('type', $type)->count();
        $totalPages = ceil($hadiths_count / $limit);
        $temp = [];
        $hadiths_ids = [];
        $hadiths = Hadith::where('type', $type)->skip(($page - 1) * $limit)->limit($limit)->get()->toArray();
        foreach ($hadiths as $key => $value) {
            $temp[$value["id"]] = $value;
            $hadiths_ids[] = $value["id"];
        }
        $hadiths = $temp;
        $pages = $this->getPhotosByHadithsIds($hadiths_ids);
        foreach ($pages as $key => $value) {
            $hadiths[$value->hadith_id]["references"][] = $value;
        }
        if ($page < $totalPages) {
            $next_page = $page + 1;
        } else {
            $next_page = false;
        }
        $temp=[];
        foreach ($hadiths as $key => $value) {
            $temp["Data"][] = $value;
        }
        $hadiths = $temp;
        $hadiths["Settings"]["total_pages"] = $totalPages;
        $hadiths["Settings"]["current_pages"] = $page;
        $hadiths["Settings"]["current_limit"] = $limit;
        $hadiths["Settings"]["total_hadiths"] = $hadiths_count;
        $hadiths["Settings"]["next_pages"] = $next_page;
        return response($hadiths, 200);
    }
    
    
    public function getHadithsByCategoryId(request $request) {
        if ($request->has('id')) {
            $id = $request->get('id');
        } else {
            return response([], 200);
        }
        if ($request->has('limit')) {
            $limit = $request->get('limit');
        } else {
            $limit = 5;
        }
        if (!$request->has('page') || $request->get('page') < 1) {
            $page = 1;
        } else {
            $page = $request->get('page');
        }
        $hadiths_count = Hadith::where('category_id', $id)->count();
        $totalPages = ceil($hadiths_count / $limit);
        $temp = [];
        $hadiths_ids = [];
        $hadiths = Hadith::where('category_id', $id)->skip(($page - 1) * $limit)->limit($limit)->get()->toArray();
        foreach ($hadiths as $key => $value) {
            $temp[$value["id"]] = $value;
            $hadiths_ids[] = $value["id"];
        }
        $hadiths = $temp;
        $pages = $this->getPhotosByHadithsIds($hadiths_ids);
        foreach ($pages as $key => $value) {
            $hadiths[$value->hadith_id]["references"][] = $value;
        }
        if ($page < $totalPages) {
            $next_page = $page + 1;
        } else {
            $next_page = false;
        }
        $temp=[];
        foreach ($hadiths as $key => $value) {
            $temp["Data"][] = $value;
        }
        $hadiths = $temp;
        $hadiths["Settings"]["total_pages"] = $totalPages;
        $hadiths["Settings"]["current_pages"] = $page;
        $hadiths["Settings"]["current_limit"] = $limit;
        $hadiths["Settings"]["total_hadiths"] = $hadiths_count;
        $hadiths["Settings"]["next_pages"] = $next_page;
        return response($hadiths, 200);
    }

    public function getHadithsByTagId(request $request) {
        if ($request->has('id')) {
            $id = $request->get('id');
        } else {
            return response([], 200);
        }
        if ($request->has('limit')) {
            $limit = $request->get('limit');
        } else {
            $limit = 5;
        }
        if (!$request->has('page') || $request->get('page') < 1) {
            $page = 1;
        } else {
            $page = $request->get('page');
        }
        $hadiths_count = TagSubscription::where('tag_id', $id)->count();
        $totalPages = ceil($hadiths_count / $limit);
        $temp = [];
        $hadiths_ids = [];
        $subscription_ids = TagSubscription::where('tag_id', $id)->skip(($page - 1) * $limit)->limit($limit)->get()->pluck('hadith_id')->toArray();
        $hadiths = Hadith::whereIn('id', $subscription_ids)->get()->toArray();
        foreach ($hadiths as $key => $value) {
            $temp[$value["id"]] = $value;
            $hadiths_ids[] = $value["id"];
        }
        $hadiths = $temp;
        $pages = $this->getPhotosByHadithsIds($hadiths_ids);
        foreach ($pages as $key => $value) {
            $hadiths[$value->hadith_id]["references"][] = $value;
        }
        if ($page < $totalPages) {
            $next_page = $page + 1;
        } else {
            $next_page = false;
        }
        $temp=[];
        foreach ($hadiths as $key => $value) {
            $temp["Data"][] = $value;
        }
        $hadiths = $temp;
        $hadiths["Settings"]["total_pages"] = $totalPages;
        $hadiths["Settings"]["current_pages"] = $page;
        $hadiths["Settings"]["current_limit"] = $limit;
        $hadiths["Settings"]["total_hadiths"] = $hadiths_count;
        $hadiths["Settings"]["next_pages"] = $next_page;
        return response($hadiths, 200);
    }

}
