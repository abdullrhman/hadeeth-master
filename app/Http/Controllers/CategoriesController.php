<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Validator;

class CategoriesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.categories.main.add');
    }

    public function createSub() {
        $categories = $this->getMainCategories();
        return view('admin.categories.sub.add')->with('categories', $categories);
    }

    public function createSection() {
        $categories = $this->getSubCategories();
        return view('admin.categories.section.add')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'name' => 'required|string',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
        }
        $category = new Category();
        if ($request->has('parent')) {
            $category->parent_id = $request->get('parent');
        }
        if ($request->has('second_parent')) {
            $id = $request->get('second_parent');
            $second_parent = Category::where('id',$id)->get()->first();
            $category->second_parent_id = $second_parent->parent_id;
            $category->parent_id = $id;
        }
        
        $category->name = $request->get('name');

        if ($category->save()) {
            return redirect()->back()->with('success', 'success');
        } else {
            return redirect()->back()->with('fail', "fail");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category) {
        //
    }

    public function getMainCategories() {
        return Category::where('parent_id', null)->where('second_parent_id', null)->get();
    }

    public function getSubCategories() {
        return Category::where('parent_id', '!=', null)->where('second_parent_id', null)->get();
    }

    public function getSectionCategories() {
        return Category::where('parent_id', '!=', null)->where('second_parent_id', '!=', null)->get();
    }

    public function getCategories() {
        $temp=[];
        $categories["data"] = Category::all()->toArray();
        foreach ($categories["data"] as $Key => $Value) {
            $temp[$Value["id"]] = $Value;
        }
        $categories["data"] = $temp;
        $temp=[];
        foreach ($categories["data"] as $Key => $Value) {
            if($Value["second_parent_id"] != null && $Value["parent_id"] != null){
                $categories["data"][$Value["parent_id"]]["sections"][]=$Value;
                unset($categories["data"][$Key]);
            }
        }
        foreach ($categories["data"] as $Key => $Value) {
            if($Value["second_parent_id"] == null && $Value["parent_id"] != null){
                $categories["data"][$Value["parent_id"]]["books"][]=$Value;
                unset($categories["data"][$Key]);
            }
        }
        $temp=[];
        foreach ($categories["data"] as $Key => $Value) {
            $temp["Data"][] = $Value;
        }
        $categories = $temp;
        return response($categories, 200);
    }

}
