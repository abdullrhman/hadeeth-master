<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Photo;
use Carbon\Carbon;
use App\SahihAlbukhari;

class PhotosController extends Controller {

    public function destroy($id) {
        if (Photo::destroy($id)) {
            return redirect()->back()->with('success', 'Delete Success');
        } else {
            return redirect()->back()->with('fail', "Delete Fail");
        }
    }

    public function creatAlbukhari() {

        $string = "uploads/images\\albukhari-";
        $ext = ".jpg";
        $number = 1944;
        $temp =$number;
        for ($i = 0; $i < $temp; $i++) {

            if ($number < 1000) {
                $string = "uploads/images\\albukhari-0";
            }

            if ($number < 100) {
                $string = "uploads/images\\albukhari-00";
            }

            if ($number < 10) {
                $string = "uploads/images\\albukhari-000";
            }

            $photos[$i]["image"] = $string . $number . $ext;
            $photos[$i]["description"] = "Sahih Albukhari Page no.".($number);
            $created_time = Carbon::now();
            $photos[$i]["updated_at"] = $created_time;
            $photos[$i]["created_at"] = $created_time;
            $number--;
        }
        $photos=array_reverse($photos);
        //SahihAlbukhari::insert($photos);
        dd($photos);
    }

}
