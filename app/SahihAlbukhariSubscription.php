<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SahihAlbukhariSubscription extends Model
{
    protected $table = "sahih_albukhari_subscription";
}
