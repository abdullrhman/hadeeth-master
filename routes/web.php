<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});


// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

//Admin Registration Routes...
//Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register')->middleware('authenticated');
//Route::post('register', 'Auth\RegisterController@register')->middleware('authenticated');
Route::get('/install/admin/{email}/{password}', 'Auth\RegisterController@installAdmin');
Route::get('/install/admin', 'Auth\RegisterController@installAdmin');


// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/home', 'HomeController@index')->name('home');

//Route::middleware(['auth', 'active.user'])->group(function () {
Route::middleware('auth')->group(function () {
    Route::get('/admin', function() {
        return view('admin.dashboard');
    });
    Route::get('/settings', 'SettingsController@resetPassword');
    Route::post('/reset/password', 'SettingsController@changePassword')->name('reset_password');



    Route::resource('/admin/hadith', 'HadithsController')->except(['update', 'edit',]);

    Route::post('/admin/hadith/store', 'HadithsController@store')->name('hadith');
    Route::get('/admin/hadith/edit/{id}', 'HadithsController@edit');
    Route::patch('/admin/hadith/update/{id}', 'HadithsController@update')->name('hadith.update');

    Route::resource('/admin/tags', 'TagsController');
    Route::post('/admin/tags/store', 'TagsController@store')->name('tag');

    Route::resource('/admin/categories/main', 'CategoriesController');
    Route::post('/admin/categories/main/store', 'CategoriesController@store')->name('category');
    Route::get('/admin/categories/sub/create', 'CategoriesController@createSub');
    Route::get('/admin/categories/section/create', 'CategoriesController@createSection');
    
    Route::get('/admin/photo/{id}', 'PhotosController@destroy');


    Route::get('/creatAlbukhari', 'PhotosController@creatAlbukhari');
    Route::get('/testMail', 'MailController@welcomeMail');
});

Route::middleware(['admin'])->group(function () {
    
});









